### nginx-proxy stack

1. [nginx-proxy](https://github.com/jwilder/nginx-proxy) acts as reverse proxy for other containers.
   
2. [acme-companion](https://github.com/nginx-proxy/acme-companion/)
   obtains certificates from [letsencrypt](https://letsencrypt.org/)
   used by the revsers proxy.

Both monitor the docker daemon via `/var/run/docker.sock` for newly created containers. Based on specific environment variables, see example below, the required configuration gets generated. 


#### Examples 
Important: Make sure the container is connected to the reverse-proxy_default network.
##### docker-compose
```
  whoami:
    image: jwilder/whoami
    container_name: whoami
    environment:
      - VIRTUAL_HOST=whoami.example.com
      - LETSENCRYPT_HOST=whoami.example.com
      - LETSENCRYPT_EMAIL=webmaster@example.com
```

##### docker run 
```
fqdn="whoami.example.com"
docker run --rm --name whoami \
           --env VIRTUAL_HOST=$fqdn \
           --env LETSENCRYPT_HOST=$fqdn \
           --network reverse-proxy_default \
           jwilder/whoami  

```


#### Mailcow
To activate the configuration for  [mailcow](https://mailcow.email/):
  - `cd` in `./data/conf.d/` and 
  - create a hardlink `ln ../mailcow_proxy.conf mailcow_proxy.conf`
  - The volume with certs obtained by mailcow is already part of docker-compose.yml, comment if not needed

#### ejabberd
To set proper headers for ejabberd:
  - `cd` in `./data/vhost.d/` and
  - create a hardlink `ln ../ejabberd-vhost im.example.com` 


#### Basic Auth
`echo "someusername:"`echo "somepassword" openssl passwd -stdin` > ./data/htpasswd/www.example.com`
to [enable basic auth for a vhost](https://github.com/jwilder/nginx-proxy#basic-authentication-support)
or use the bash script in `./utils/setup_basic-auth.sh`

#### Notes and pitfalls
When changing domains while moving from development to production, perform a `docker system prune` prior to restarting the service.
