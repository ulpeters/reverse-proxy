#!/bin/bash

#---Objective---
# Replicates keys to another directory and make them world readable
# so other containers can make use of them.

#---Context---
# acme-companion sets the root user as the key owner 
# and removes read permission for other users.
# Other containers run under unprivileged user IDs, e.g. 1000 or 9000,
# and cannot access the keys.

# Define source and destination directories
src_dir="/etc/nginx/certs/"
dest_dir="/etc/nginx/certs-shared/"
mkdir -p $dest_dir

# Copy the files and update permissions
cp --dereference --update -p $src_dir*.key $src_dir*.crt $dest_dir
chmod -R a+r $dest_dir

# Delete files in the destination that are not in the source
for file in $dest_dir*; do
  base_file=$(basename "$file")
  if [[ ! -e $src_dir$base_file ]]; then
    rm "$file"
  fi
done
