#!/bin/bash -e
[ $# -eq 0 ] && { echo "Usage: $0 <vhost> <user> <pass>"; exit 1; }
vhost=$1
user=$2
pass=$3
htpasswd_dir=/opt/docker/reverse-proxy/data/htpasswd/

mkdir -p $htpasswd_dir 

htpasswd="$user"":"`echo $pass | openssl passwd -stdin`
echo "  Writing:"
echo "    --> $htpasswd"
echo "    --> into: $htpasswd_dir$vhost"
echo $htpasswd > $htpasswd_dir$vhost

# Restart so the password file is read by nginx
docker restart reverse-proxy
