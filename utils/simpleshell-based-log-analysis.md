## Simple shell-based log analysis 



#### Define hostname
```
vhost_name="www\.example\.com"
```

#### Group by IP since 24h
```
docker logs reverse-proxy --since 24h 2>&1 \
| grep -e $vhost_name \
| grep -oE "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b" \
| sort \
| uniq -c
```


#### Resolve IPs
```
for ip in `
  docker logs reverse-proxy 2>&1 \
  | grep -e $vhost_name \
  | grep -oE "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b" \
  | sort \
  | uniq`; do echo "$ip" `dig -x $ip +short`; done
```

#### Visites sites
```
docker logs reverse-proxy 2>&1 \
  | grep -e $vhost_name \
  | grep -oE '[a-z]*.php' \
  | sort \
  | uniq -c
```